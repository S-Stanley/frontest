# Frontest

The end goal of frontest is to allow software engineer to make end2end front-end
test in a simple, better way.

The main idea is to just allow them to write test by clicking on a webpage.

- [ ] Evaluate the problem doing discovery-interviews (at least 10 interviews).
- [ ] Creating some mockups with figma and evaluate the idea and the pricing (with a freemium version if possible).
- [ ] Opening a waiting list and use social media to increase the list.
- [ ] Open beta testing with somes developers.
- [ ] Open globally to any software engineer
